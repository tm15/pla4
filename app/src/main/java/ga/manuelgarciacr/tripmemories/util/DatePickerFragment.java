package ga.manuelgarciacr.tripmemories.util;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;

public class DatePickerFragment extends DialogFragment {
    private final String ARG_DATE = "date";

    public DatePickerFragment newInstace(Date date) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_DATE, date);
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        DatePickerDialog.OnDateSetListener dateListener = (view, year, month, day) -> {
            Date resultDate = new GregorianCalendar(year, month, day).getTime();
            DatePickerFragment.Callbacks frag = (DatePickerFragment.Callbacks) getTargetFragment();
            assert frag != null;
            frag.onDateSelected(resultDate);
        };
        assert getArguments() != null;
        Date date = (Date) getArguments().getSerializable(ARG_DATE);
        Calendar calendar = Calendar.getInstance();
        assert date != null;
        calendar.setTime(date);
        int initialYear = calendar.get(Calendar.YEAR);
        int initialMonth = calendar.get(Calendar.MONTH);
        int initialDay = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(Objects.requireNonNull(getContext()), dateListener, initialYear, initialMonth,
                initialDay);
    }

    public interface Callbacks {
        void onDateSelected(Date date);
    }
}
